$(document).ready(function(){	

	var w2 = ($(window).width()-$('.cabinet_personal .main').width())/2;
	$('.left_bg').css('width', w2);
	$('.left_bg').css('left', -w2);	
	
	function start(){		
		if ($(window).width()<=1200){			
			$('.list_main_mews > li').addClass('active');			
			$( ".list_main_mews > li" ).each(function( index ) {
				$(this).css('background-image','url('+$(this).data('img')+')');			
			});
		}
	
		if ($(window).width()<=990){
			//Карусель
			if ($('.list_managers > li').length>1){
				var owl = $('.list_managers');
				owl.owlCarousel({
					margin: 20,
					nav: false,
					dots: true,
					loop: true,
					center: true,
					items: 2,
					responsive: {
					  0: {				
						items: 1,
						center: false
					  },
					  520: {				
						items: 2,
						center: true
					  },
					  769: {
						items: 3,
						center: false
					  }
					}
				});
			}
		}
	
		if ($(window).width()<=767){
			$('.listing_block').addClass('hide_filter');
			$('a.lf_btn').find('span').text($('a.lf_btn').data('close'));
			$('a.lf_btn').removeClass('active');
			$('.products').removeClass('G4');
			$('.products').addClass('G3');
			$('.G3').addClass('active');
			$('.G4').removeClass('active');
						
			$('.cabinet_personal .main > .cab_sidebar').appendTo($('.cab_header'));
		} else {			
			if($('.cab_header > .cab_sidebar').length > 0){
				$('.cab_header > .cab_sidebar').prependTo($('.cabinet_personal .main'));
			}			
		}
		
		if ($(window).width()<=990){
			$('.listing_product .view_panel .for_lf_btn').appendTo($('.view_panel'));
		} else {
			$('.listing_product .view_panel .for_lf_btn').prependTo($('.view_panel'));
		}
		$('.nav_filter').css('max-height',$(window).height()-$('header > .main').height());
	}
	start()	
	$(window).resize(function(){
		start();		
	});
	
	$('input, select').styler({
		selectSearch: true
	});
	
	// маска
	$('input[name="phone"]').mask("+7 (999) 999-99-99");
	
	// Поп-ап
	$('.popup').fancybox({
		padding : 0,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});
	$('.popup_video').fancybox({
		type: "iframe",
		padding : 0,
		helpers: {
			overlay: {
				locked: false
			}
		}
	});
	
	if ($('#slider-range').length>0){
		$("#slider-range").slider({
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: 0,
			max: 1440,
			step: 1,
			value: 700,
			tooltip: 'always',
			disabled: true,
			create: function() {
				var hours1 = Math.floor(700 / 60);
				var minutes1 = 700 - (hours1 * 60);
				if (hours1 >= 12) {
					if (hours1 == 12) {
						hours1 = hours1;
						minutes1 = minutes1 + "";
					} else {
						hours1 = hours1 - 12;
						minutes1 = minutes1 + "";
					}
				} else {
					hours1 = hours1;
					minutes1 = minutes1 + "";
				}
				if (hours1 == 0) {
					hours1 = 12;
					minutes1 = minutes1;
				}
				if (hours1 < 10) hours1 = '0' + hours1;
				if (hours1.length == 1) hours1 = '0' + hours1;
				if (minutes1.length == 1) minutes1 = '0' + minutes1;
				if (minutes1 == 0) minutes1 = '00';
				$('.ui-slider-handle').html('');
				$('.ui-slider-handle').append('<span></span>');
				$('.ui-slider-handle span').html(hours1 + ':' + minutes1);
			}
		})
	}	
	
	//Карусель
	if ($('.main_slider .item').length>1){
		var owl = $('.main_slider');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			dots: true,
			loop: true,
			items: 1,
			responsive: {
			  0: {				
				nav: false,
				dots: true
			  },
			  991: {
				nav: true,
				dots: true
			  }
			}
		});
	}
	
	//Карусель
	if ($('.big_gallery_slider .item').length>1){
		var owl = $('.big_gallery_slider');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			dots: true,
			loop: true,
			items: 1,
			responsive: {
			  0: {				
				nav: false,
				dots: true
			  },
			  991: {
				nav: true,
				dots: true
			  }
			}
		});
	}
	
	//Карусель
	if ($('.video_gallery_slider .item').length>1){
		var owl = $('.video_gallery_slider');
		owl.owlCarousel({
			margin: 30,
			nav: false,
			dots: true,
			loop: true,
			center: true,
			items: 4,
			responsive: {
			  0: {				
				items: 1,
			  },
			  768: {				
				items: 2,
			  },
			  991: {
				items: 4,
			  }
			}
		});
	}
	
	//Карусель
	if ($('.s_slider .item').length>1){
		var owl = $('.s_slider');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			dots: true,
			loop: true,
			center: true,
			items: 1,
			responsive: {
			  0: {				
				nav: false,
				dots: true
			  },
			  768: {				
				nav: true,
				dots: true
			  }
			}
		});
	}
	
	//Карусель
	if ($('.category_cars_listing .item').length>1){
		var owl = $('.category_cars_listing');
		owl.owlCarousel({
			margin: 0,
			nav: true,
			dots: false,
			loop: true,
			items: 5,
			responsive: {
			  0: {
				items: 1
			  },
			  412: {
				items: 2
			  },
			  768: {
				items: 3
			  },
			  991: {
				items: 4
			  },
			  1200: {
				items: 5
			  }
			}
		});
	}													   
	
	if ($('.instagram_photos').length>0){
		var instagram_photos = $('.instagram_photos');
		instagram_photos.owlCarousel({
			margin: 0,
			nav: false,
			dots: false,
			loop: true,
			center: true,
			responsive: {
			  0: {
				items: 2
			  },
			  480: {
				items: 4
			  },
			  769: {
				items: 6
			  },
			  1191: {
				items: 8
			  }
			}			
		});
	}

	
	if ($('.product_related').length>0){
		var product_related = $('.product_related');
		product_related.owlCarousel({
			margin: 0,
			nav: true,
			dots: false,
			loop: true,
			responsive: {
			  0: {
				items: 1,
				nav: false,
				dots: true
			  },
			  560: {
				items: 2,
				nav: false,
				dots: true
			  },
			  769: {
				items: 3,
				nav: false,
				dots: true
			  },
			  1340: {
				items: 4,
				nav: true,
				dots: false
			  }
			}
		});
	}	
			 
	// Отключаем все формы
	$('form').submit(function() {
		//return false;
	});	
	
	// Проверка на правильность е-майла
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	}
	
	//Отправка на почту
	function SEND_mail(s_name, s_phone, s_email, s_comment, s_product, s_temp, s_subject, s_text, s_url_send, s_view_name, s_view_phone, s_view_mail, s_view_comment, s_view_product, s_view_temp){
		
		// Имя
		if (s_name == 'no_name'){
		   var name = s_name;
		   var real_name = false;		
		} else{
		   var name = $(s_name).val();
		   var real_name = true;				
		}	
		
		// Телефон
		if (s_phone == 'no_phone'){
		   var phone = s_phone;
		   var real_phone = false;		
		} else{
		   var phone = $(s_phone).val();
		   var real_phone = true;				
		}
		
		// Почта
		if(s_email.indexOf('@') + 1) {
		   var email = s_email;
		   var real_mail = false;
		}else{
		   var email = $(s_email).val();
		   var real_mail = true;
		}
		
		// Комментарий
		if (s_comment == 'no_comment'){
		   var comment = s_comment;
		   var real_comment = false;		
		} else{
		   var comment = $(s_comment).val();
		   var real_comment = true;				
		}
		
		// Продукт
		if (s_product == 'no_product'){
		   var product = s_product;
		   var real_product = false;		
		} else{
		   var product = $(s_product).val();
		   var real_product = true;				
		}
		
		// Доп поле
		if (s_temp == 'no_temp'){
		   var temp = s_temp;
		   var real_temp = false;		
		} else{
		   var temp = $(s_temp).val();
		   var real_temp = true;				
		}
		
		var subject = s_subject;
		var text = s_text;
		
		var view_name = s_view_name;
		var view_phone = s_view_phone;
		var view_mail = s_view_mail;
		var view_comment = s_view_comment;
		var view_product = s_view_product;
		var view_temp = s_view_temp;
		
		if(name == '' || phone == '' || email == ''){alert('Заполните все поля.');}
		else if (view_comment == 'yes_faq' && comment == '') {alert('Укажите вопрос.');}
		else if (isValidEmailAddress(email))
		{ 
			$.ajax({
			 type: "POST",
			 url: s_url_send,
			 cache: false,
			 data: "name="+name+"&phone="+phone+"&email="+email+"&comment="+comment+"&product="+product+"&temp="+temp+"&subject="+subject+"&text="+text+"&view_name="+view_name+"&view_phone="+view_phone+"&view_mail="+view_mail+"&view_comment="+view_comment+"&view_product="+view_product+"&view_temp="+view_temp,
			 success: function(answ){
				if (real_name == true) $(s_name).val('');
				if (real_phone == true) $(s_phone).val('');
				if (real_mail == true) $(s_email).val('');
				if (real_comment == true) $(s_comment).val('');
				if (real_product == true) $(s_product).val('');
				if (real_temp == true) $(s_temp).val('');

				
				$( ".fancybox-close" ).trigger( "click" );
				setTimeout( function() { 
					window.location = "/spasibo";
				} , 501);
				/*setTimeout( function() { $( ".sankyou" ).trigger( "click" ); } , 501);
				setTimeout( function() { $( ".fancybox-close" ).trigger( "click" ); } , 4000);*/
				
			 }
			});
		}
		else
		{
			alert('Неверный E-mail');
		}
	}
	
	$('.send_z').click(function(){ 
		SEND_mail(
			'#name_z', 			// id поля имя или no_name
			'#phone_z', 		// id поля телефон или no_phone
			'info@labor.ru', 			// id поля телефон или ZAKAZ@nomail.ru (можно любой другой)
			'no_comment', 		// id поля комментария или no_comment
			'no_product', 		// id поля продукта или no_product
			'#company_z', 			// id доп. поля 
			'Заказ звонка', 	// Заголовок письма
			'Заказ звонка с сайта labor.ru ', 	// Заголовок в теле письма
			'ajax/send_mail.php', 	// php-файл для отправки на почту
			'yes_name', 		// выводим в письме имя (yes_name/no_name)
			'yes_phone', 		// выводим в письме телефон (yes_phone/no_phone)
			'no_mail', 		// выводим в письме почту (yes_mail/no_mail)
			'no_comment', 		// выводим в письме комментарий (yes_comment/no_comment)
			'no_product', 		// выводим в письме продукт (yes_product/no_product)
			'yes_company' 			// выводим в письме доп поле (yes_temp/no_temp)
		); 
	});
	
	$('.tab_variants_nav > li > a').click(function(e){ 
		e.preventDefault();
		$('.tab_variants_nav > li').removeClass('active');
		$('.tab_variants_content > .vc_data').removeClass('active');
		$(this).parent().addClass('active');
		var index = $(this).parent().index();
		$('.tab_variants_content > .vc_data').eq(index).addClass('active');
	});
	
	$('.faq > li > a').click(function(e){ 
		e.preventDefault();
		$(this).parent().toggleClass('active');
		$(this).next().slideToggle();
	});
	
	
	$( ".list_main_mews > li" )
	.mouseenter(function() {
		$(this).css('background-image','url('+$(this).data('img')+')');
		$(this).css('background-position','center center');
		$(this).css('background-size','cover');
		$(this).css('background-repeat','no-repeat');
	})
	.mouseleave(function() {
		$(this).css('background','#fff');
	});

	
	
	$( ".product_head .pp_variants li:not(.other)" ).mouseenter(function() {
		$(this).closest('.pp_variants').find('li').removeClass('active');
		var color = $(this).data('color');
		$('.product_head .r_slider .owl-carousel .owl-item.active img.'+color).attr('src',$('.product_head .r_slider .owl-carousel .owl-item.active img').data(color));

		$('.product_head .r_slider .owl-carousel .owl-item.active img.'+color).attr('data-zoom-image',$('.product_head .r_slider .owl-carousel .owl-item.active img').data(color+'_zoom'));
		$('.product_head .r_slider .owl-carousel .owl-item.active img.'+color).removeData('zoom-image');
		
		$('.zoomContainer').remove();
		$('.product_head .r_slider .owl-item.active .zoom > img').elevateZoom({
			cursor: "crosshair",
			zoomWindowFadeIn: 0,
			zoomWindowFadeOut: 0,
			zoomWindowWidth:560,
			zoomWindowHeight:560,				
			zoomWindowPosition: "pp_right", 
			borderSize: 0, 
			easing:true
		});
		
		$(this).addClass('active');
	});

	var product_head_slider = $('.product_head .r_slider > ul');
	product_head_slider.owlCarousel({
		margin: 0,
		nav: false, 
		dots: false,
		loop: false,	
		URLhashListener:true,
		autoplayHoverPause:true,
		startPosition: 'URLHash',
		video:true,
		lazyLoad:true,
		items: 1,
		singleItem: true,
		responsive: {
		  0: {
			nav: false, 
			dots: true
		  },
		  480: {
			nav: false, 
			dots: true
		  },
		  769: {
			nav: false, 
			dots: true
		  },
		  1191: {
			nav: false, 
			dots: false
		  }
		}
	});	
	
	if ($(window).width()>1190){
		if($('.product_head .r_slider .owl-item.active .zoom > img').length > 0){
			$('.product_head .r_slider .owl-item.active .zoom > img').elevateZoom({
				cursor: "crosshair",
				zoomWindowFadeIn: 0,
				zoomWindowFadeOut: 0,
				zoomWindowWidth:560,
				zoomWindowHeight:560,				
				zoomWindowPosition: "pp_right", 
				borderSize: 0, 
				easing:true
				
			});
		}
	}
	product_head_slider.on('initialized.owl.carousel', function(event) {
		if($('.product_head .r_slider .owl-carousel').find('.owl-item.active .for_video_container video').length>0){
			$('.product_head .r_slider .owl-carousel').find('.owl-item.active .for_video_container video').get(0).play();
		}
		
		
		
	});
	product_head_slider.on('translated.owl.carousel', function(event) {
		$('.product_head .r_slider .owl-carousel').find('.owl-item .for_video_container video').get(0).pause();
		if($('.product_head .r_slider .owl-carousel').find('.owl-item.active .for_video_container video').length>0){
			setTimeout( function() { 
				$('.product_head .r_slider .owl-carousel').find('.owl-item.active .for_video_container video').get(0).play();
			}, 1);
		}
		var ihash = 0;
		if($('.product_head .r_slider .owl-carousel').find('.owl-item').length>0){
			ihash = $('.product_head .r_slider .owl-carousel .owl-item.active > li').data('hash');
			$('.product_head').find('.any-class ul li > a').removeClass('active');
			$('.product_head').find('.any-class ul li > a[href="#'+ihash+'"]').addClass('active');
		}
		if ($(window).width()>1190){
			$('.zoomContainer').remove();
			if($('.product_head .r_slider .owl-item.active .zoom > img').length > 0){
				setTimeout( function() { 
					$('.product_head .r_slider .owl-item.active .zoom > img').elevateZoom({
						cursor: "crosshair",
						zoomWindowFadeIn: 0,
						zoomWindowFadeOut: 0,
						zoomWindowWidth:560,
						zoomWindowHeight:560,				
						zoomWindowPosition: "pp_right", 
						borderSize: 0, 
						easing:true
					});
				} , 1);
			}
		}
	})
	
	if ($('.vp_slider').length>0){
		$('.vp_slider').on('click', 'ul li > a', function(e){
			// e.preventDefault();				
			$('.vp_slider .any-class ul li > a').removeClass('active');
			$(this).addClass('active');
		});	
		
		$(".vp_slider .any-class").jCarouselLite({
			btnNext: ".vp_slider .next",
			btnPrev: ".vp_slider .prev",
			visible: 5,
			vertical: true
		});
	}
	
	if ($('#lightgallery').length>0){
		$('#lightgallery').lightGallery();
	}
		
	$('.p_zoom').on('click', function(e){
		e.preventDefault();		
		var slide_n = $('.r_slider .owl-carousel .owl-item.active > li').data('hash');
		$('#lightgallery > li.'+slide_n).trigger('click');
	});
	
	$( ".product_in_popup .pp_variants li:not(.other)" ).mouseenter(function() {
		$(this).closest('.pp_variants').find('li').removeClass('active');
		var color = $(this).data('color');
		$('.product_in_popup .r_slider .owl-carousel .owl-item img').removeClass('active');
		$('.product_in_popup .r_slider .owl-carousel .owl-item img.'+color).addClass('active');
		$(this).addClass('active');
	});

	
	$( ".pp_variants li:not(.other)" ).click(function(e) {
		e.preventDefault();
		$(this).closest('.pp_variants').find('li').removeClass('active');
		$(this).addClass('active');
		$(this).closest('.pp_variants').next('input[type="hidden"]').val($(this).data('variant'));
	});
	
	$( ".pp_variants li.other" ).click(function(e) {
		e.preventDefault();
		//$(this).hide(300);
		if($(this).hasClass('open')==false){
			$(this).addClass('open');
			$(this).find('a').text($(this).data('hide'));		
			$(this).closest('ul').find('li.hidden').css('display','inline-block').show(300);
		} else {
			$(this).removeClass('open');
			$(this).find('a').text($(this).data('show'));		
			$(this).closest('ul').find('li.hidden').css('display','inline-block').hide(300);			
		}
	});	
	
	$('.pp_type > li > a').click(function(e){
		e.preventDefault();				
		$('.pp_type > li').removeClass('active');
		$(this).parent().addClass('active');
	});	
	
	$('.pp_number .minus').click(function () {
		var $input = $(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		return false;
	});
	$('.pp_number .plus').click(function () {
		var $input = $(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();
		return false;
	});
	
	$('.pd_nav > li > a').click(function(e){
		e.preventDefault();				
		$('.pd_nav > li').removeClass('active');
		$('.pd_desc > .item').removeClass('active');
		$(this).parent().addClass('active');
		var index = $(this).parent().index();
		$('.pd_desc > .item').eq(index).addClass('active');
	});
	
	$('.lp_type > li > a').click(function(e){
		e.preventDefault();				
		$('.lp_type > li').removeClass('active');
		$(this).parent().addClass('active');
	});

	$( ".p_variants li:not(.other)" ).mouseenter(function() {
		$(this).closest('.p_variants').find('li').removeClass('active');
		$(this).closest('div').find('.p_img img').attr('src',$(this).find('a').data('img'));
		$(this).addClass('active');
	});
	$( ".p_variants li.other a" ).on('click', function(e){
		e.preventDefault();
		$(this).closest('ul').find('.h').css('display','inline-block');
		$(this).closest('li').hide(0);		
	});
	$(".ipr_item > ul > li, .ipr_item > ul .owl-stage > .owl-item > li").mouseleave(function(){
		$(this).find('.p_variants li.h').hide();
		$(this).find('.p_variants li.other').css('display','inline-block');
	});
	
	$(".products .ipr_item").mouseleave(function(){
		$(this).find('.p_variants li.h').hide();
		$(this).find('.p_variants li.other').css('display','inline-block');
		$(this).find('.p_size li.h').hide();
		$(this).find('.p_size li.other').css('display','inline-block');
	});
		
	$('.p_variants li a').on('click', function(e){
		e.preventDefault();			
	});	
	
	$(window).scroll(function(){
		if ( $(window).scrollTop() > 30 ){
			$('body').addClass('no_top');
		} else {
			$('body').removeClass('no_top');
		}
		
		$('.p_variants li.h').hide();
		$('.p_variants li.other').css('display','inline-block');
		
	});
	
	$('.sort_top > div > a').on('click', function(e){
		e.preventDefault();				
		$(this).closest('.sort_top').find('ul').slideToggle(250);
		$(this).closest('.sort_top').toggleClass('open');
	});
	
	$('.sort_top > ul > li > a').on('click', function(e){
		e.preventDefault();		
		$(this).closest('.sort_top').find('ul').slideUp(250);
		$(this).closest('.sort_top').removeClass('open');		
	});	
	
	$("body, html").click(function (event) {
		if ($(event.target).closest(".sort_top").length === 0) {
			$('.sort_top').removeClass('open');				
			$('.sort_top ul').slideUp(120);				
		}
	});
	
	$('a.G4').on('click', function(e){
		e.preventDefault();		
		$('a.G3').addClass('active');
		$('a.G4').removeClass('active');
		$('.products').addClass('G3');
		$('.products').removeClass('G4');
	});	
	
	$('a.G3').on('click', function(e){
		e.preventDefault();		
		$('a.G3').removeClass('active');
		$('a.G4').addClass('active');
		$('.products').removeClass('G3');
		$('.products').addClass('G4');
	});	
	
	$('.filters > li > a').on('click', function(e){
		e.preventDefault();		
		$(this).parent().toggleClass('open');
		$(this).next().slideToggle(300);
	});
	
	$('.btn_cab_nav').on('click', function(e){
		e.preventDefault();		
		$(this).toggleClass('active');
		$(this).next().slideToggle(300);
	});	
	
	$('.cc_more .btn').on('click', function(e){
		e.preventDefault();		
		$(this).closest('.cab_cart_block').toggleClass('open');
		
		if($(this).closest('.cab_cart_block').hasClass('open')){
			$(this).closest('.cab_cart_block').find('.cc_body').slideDown();
			$(this).text($(this).data('close'));
		} else {
			$(this).closest('.cab_cart_block').find('.cc_body').slideUp();
			$(this).text($(this).data('open'));			
		}
	});	
	
	if ($(window).width()>1200){
		if($('.parallax').length>0){
			$('.parallax').parallax();
		}
	}
	
	$('a.lf_btn').on('click', function(e){
		e.preventDefault();		
		if($(this).hasClass('active')){
			$('.listing_block').addClass('hide_filter');
			$(this).find('span').text($(this).data('close'));
			$(this).removeClass('active');
		} else {
			$('.listing_block').removeClass('hide_filter');
			$(this).find('span').text($(this).data('open'));
			$(this).addClass('active');
			/*else {
				$('.listing_block').addClass('mob_show');
				$('.left_nav > ul > li').removeClass('open');
				$('.left_nav.as_filter > ul > li > .block_filter').hide();
				$('.left_nav > ul > li > ul').hide();
			}*/
		}		
	});
	
	$('.nav_btn > a').on('click', function(e){
		e.preventDefault();		
		$('.nav_filter').slideToggle();	
	});
	
	$('.btn_search').click(function (e) {
		e.preventDefault();	
		$('.sfields').fadeIn(200);
		$('.nav_filter').hide(0);		
	});
	
	$('.sfields input.search_close').click(function (e) {
		e.preventDefault();			
		$('.sfields').fadeOut(200);		
	});
	
	$("body, html").click(function (event) {
		if ($(event.target).closest(".header_search").length === 0) {
			$('.sfields').hide(120);				
		}
	});
	
});